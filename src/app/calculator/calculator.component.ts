import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  constructor() { }

  NumbersChoices: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9]
  OperationsChoices: string[] = ["+", "-", "*", "/", "C", "<", "="]
  currentNumber: string = '';
  currentOperation: string = '';
  lastChoiceOperation: boolean = false;
  choseNumber (number: number) {
    this.currentOperation += `${number}`
    this.lastChoiceOperation = false;
  }
  doOperation (operation: string) {
    if (operation === "=") {
      this.currentNumber = eval(this.currentOperation);
    }
    else if (operation === "C") {
      this.currentOperation = "";
      this.currentNumber = "";
    } else if (operation === "<") {
      if (this.OperationsChoices.includes(this.currentOperation.slice(-1))) {
        this.lastChoiceOperation = false;
      }
      this.currentOperation = this.currentOperation.slice(0, -1);
      if (this.OperationsChoices.includes(this.currentOperation.slice(-1))) {
        this.lastChoiceOperation = true;
      }
    } else if (!this.lastChoiceOperation) {
      this.currentOperation = this.currentOperation + `${operation}`
      this.currentNumber = "";
      this.lastChoiceOperation = true;
    }
  }


  ngOnInit(): void {
  }

}
