import {Component, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  API = 'https://deckofcardsapi.com/api/deck/new/draw/?count=10 ';
  cardsContainer?: any;
  cardsList?: any;
  gameInitialized: boolean = false;
  gameStarted: boolean = false;
  gameOver: boolean = false;
  countdownToGame: any;
  difficultyTimer : number = 3000;
  firstCardSelected: any;
  secondCardSelected: any;
  currentScore:number = 0;
  highScore:number = 0;
  failNumber:number = 0;
  previousCorrectCards: any;
  cardsChanger: any;
  //
  gameInit (difficultyTime: number) {
    this.gameInitialized = true;
    this.difficultyTimer = difficultyTime;
    this.countdownToGame = 3;
    this.failNumber = 0;
    this.currentScore = 0;
    let countdownInterval = setInterval( () => {
      if (this.countdownToGame === "Start") {
        clearInterval(countdownInterval)
        this.getCards()
      } else {
        this.countdownToGame --;
      }
      if (this.countdownToGame === 0) {
        this.countdownToGame = "Start";
      }
    },1000)
  }
  //
  @HostListener('window:keydown.space', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (this.gameStarted) {
      if (this.firstCardSelected.code === this.secondCardSelected.code && this.firstCardSelected.code !== this.previousCorrectCards) {
        this.currentScore ++;
        this.previousCorrectCards = this.firstCardSelected.code;
      } else if (this.firstCardSelected.code !== this.secondCardSelected.code && !this.gameOver) {
        this.failNumber ++;
      }
      if (this.failNumber === 5) {
        clearInterval(this.cardsChanger);
        this.gameOver = true;
        if (this.currentScore > this.highScore) {
          localStorage.setItem('speedHighScore', this.currentScore.toString());
          this.highScore = this.currentScore
        }

      }
    }
  }
  //
  public async getCards () {
    this.cardsContainer = await fetch(this.API).then((r) => (r.json())
    );
    this.cardsList = this.cardsContainer.cards
    this.firstCardSelected = this.cardsList[Math.floor(Math.random() * this.cardsList.length)];
    this.secondCardSelected = this.cardsList[Math.floor(Math.random() * this.cardsList.length)];
    this.gameStarted = true;
    this.cardsChanger = setInterval(() => {
      this.firstCardSelected = this.cardsList[Math.floor(Math.random() * this.cardsList.length)];
      this.secondCardSelected = this.cardsList[Math.floor(Math.random() * this.cardsList.length)];
    }, this.difficultyTimer)
  }
  //
  startAgain () {
    this.gameOver = false;
    this.gameInitialized = false;
    this.gameStarted = false;
    this.gameInit(this.difficultyTimer);
  }

  changeDifficulty () {
    this.gameOver = false;
    this.gameInitialized = false;
    this.gameStarted = false;
    clearInterval(this.cardsChanger);
  }
  //
  constructor() { }

  ngOnInit(): void {
    const speedHighScoreStorage = localStorage.getItem('speedHighScore');
    if (speedHighScoreStorage === null) {
      this.highScore = 0;
    } else {
      this.highScore = parseInt(speedHighScoreStorage)
    }
  }

}
