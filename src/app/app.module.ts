import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {GameComponent} from './game/game.component';
import {DragDropModule} from "@angular/cdk/drag-drop";
import {HomeComponent} from './home/home.component';
import {TopNavComponent} from './top-nav/top-nav.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from "./app-routing.module";
import { CalculatorComponent } from './calculator/calculator.component';
import { PokedexComponent } from './pokedex/pokedex.component';

@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    HomeComponent,
    TopNavComponent,
    CalculatorComponent,
    PokedexComponent
  ],
  imports: [
    BrowserModule,
    DragDropModule,
    RouterModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
