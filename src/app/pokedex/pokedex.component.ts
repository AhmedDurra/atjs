import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.css']
})
export class PokedexComponent implements OnInit {

  POKEMON_API = 'https://pokeapi.co/api/v2/pokemon?limit=151';
  POKEMON_IMAGES = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png';
  pokemonAPIData: any;
  PokemonData: {name: string}[] = [];

  async catchPokemons () {
    const results = await fetch(this.POKEMON_API)
    this.pokemonAPIData = await results.json()
    this.PokemonData = this.pokemonAPIData.results;
  }

  constructor() { }

  ngOnInit(): void {
    this.catchPokemons();
  }

}
